audioIn = audioread('../../a1.wav');
audioIn = sum(audioIn, 2) / size(audioIn, 2);
audioIn = audioIn(110e3:135e3,:);
timeVector = linspace(0,(25/44.1),numel(audioIn));
plot(timeVector,audioIn);
axis([0 (25/44.1) -1 1]);
ylabel('Amplitude');
xlabel('Time (s)');
title('Utterance - Two')