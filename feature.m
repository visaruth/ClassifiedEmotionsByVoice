Tw = 25;                % analysis frame duration (ms)
Ts = 10;                % analysis frame shift (ms)
alpha = 0.97;           % preemphasis coefficient
M = 20;                 % number of filterbank channels 
C = 12;                 % number of cepstral coefficients
L = 22;                 % cepstral sine lifter parameter
LF = 300;               % lower frequency limit (Hz)
HF = 3700;              % upper frequency limit (Hz)
[y, fs] = audioread('../a1.wav');
yMono = sum(y, 2) / size(y, 2);

[ MFCCs, FBEs, frames ] = ...
                    mfcc( yMono, fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L );

m1 = MFCCs(1,1:100);
disp(MFCCs(3,:));