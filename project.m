Tw = 25;                % analysis frame duration (ms)
Ts = 10;                % analysis frame shift (ms)
alpha = 0.97;           % preemphasis coefficient
M = 20;                 % number of filterbank channels 
C = 12;                 % number of cepstral coefficients
L = 22;                 % cepstral sine lifter parameter
LF = 300;               % lower frequency limit (Hz)
HF = 3700;              % upper frequency limit (Hz)
[y, fs] = audioread('c5.wav');
yMono = sum(y, 2) / size(y, 2);

[ MFCCs, FBEs, frames ] = ...
                    mfcc( yMono, fs, Tw, Ts, alpha, @hamming, [LF HF], M, C+1, L );

m1 = mean(MFCCs(1,1:100));
m2 = mean(MFCCs(2,1:100));
m3 = mean(MFCCs(3,1:100));
m4 = mean(MFCCs(4,1:100));
m5 = mean(MFCCs(5,1:100));
m6 = mean(MFCCs(6,1:100));
m7 = mean(MFCCs(7,1:100));
m8 = mean(MFCCs(8,1:100));
m9 = mean(MFCCs(9,1:100));
m10 = mean(MFCCs(10,1:100));
m11 = mean(MFCCs(11,1:100));
m12 = mean(MFCCs(12,1:100));
m13 = mean(MFCCs(13,1:100));