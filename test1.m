audioIn = audioread('../a1.wav');
audioIn = sum(audioIn, 2) / size(audioIn, 2);
timeVector = linspace(0,(25/44.1),numel(audioIn));
plot(timeVector,audioIn);
axis([0 (25/44.1) -1 1]);
ylabel('Amplitude');
xlabel('Time (s)');
title('Utterance - Two')
pD = audiopluginexample.SpeechPitchDetector;
[~,pitch] = process(pD,audioIn);

figure;
subplot(2,1,1);
plot(timeVector,audioIn);
axis([0 (25/44.1) -1 1])
ylabel('Amplitude')
xlabel('Time (s)')
title('Utterance - Two')

subplot(2,1,2)
plot(timeVector,pitch,'*')
axis([0 (25/44.1) 80 140])
ylabel('Pitch (Hz)')
xlabel('Time (s)');